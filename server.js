/** Récupération du fichier de configuration */
require('dotenv').config();

/** Import des dépendances pour le serveur web  */
const express = require('express');

/** Import des routes de l'api */
const sauceRoutes = require('./routes/sauce');
const userRoutes = require('./routes/user');
const uploadRoutes = require('./routes/upload');

/** Création du serveur web */
const app = express();

/** Paramétrage des en-têtes de sécurité */
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT,DELETE');
  next();
});

// Analyse le contenu de la requête, pour les éléments json
app.use(express.json());

// Analyse le contenu de la requête, pour les différentes données
app.use(express.urlencoded({ extended: true }));

/** Utilisation des routes pour l'api */
app.use('/api/auth', userRoutes);
app.use('/api/sauces', sauceRoutes);
/** Route pour la récupération des images, avec authentification */
app.use('/uploads', uploadRoutes);

/** Route par défaut de l'api, retourne le fichier index.html dans le dossier public */
app.get('/', function (req, res) {
  res.sendFile(__dirname + "/public/index.html");
});


//Démarrage du serveur
const PORT = process.env.PORT || 3000
const server = app.listen(PORT, function () {
  console.log("Listening on port " + PORT);
});

module.exports = server;
