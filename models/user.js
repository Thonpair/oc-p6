/** Modèle pour la gestion des sauces dans une base de données MongoDB */

/** Import des dépendances */
const mongoose = require('mongoose')
const { Schema } = mongoose;
const shajs = require('sha.js')
require('dotenv').config();

/** Connexion à la base de données */
mongoose.connect(process.env.DB, { useNewUrlParser: true, useUnifiedTopology: true });

/** Création du schéma pour les utilisateurs */
const userSchema = new Schema({
    email: String,
    password: String
})

/** Controle que l'adresse e-mail soit correctement formatée */
function checkEmail(email) {
    const emailRegex =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return (emailRegex.test(email))
}

/** Controle du mot de passe */
function checkPassword(password) {
    /** Au moins 1 lettre majuscule, 1 lettre minuscule, 1 chiffre, 1 caractère spécial, longueur minimum 8 caractères */
    const passwordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[_\W]).{8,}$/
    return (passwordRegex.test(password))
}

/** Ajoute un "salt" au mot de passe avant de le chiffrer
 * 
 * Le salt est une chaine de caractères aléatoire qui est ajoutée
 * au mot de passe avant de le chiffrer, ce qui permet de bloquer une attaque par brute-force
 * ou par dictionnaire, en cas de compromission de la table contenant les utilisateurs.
 * 
 * Le chiffrement utilisé est sha256, qui est l'un des algorithmes recommandés par la CNIL : 
 * https://www.cnil.fr/fr/securite-chiffrer-garantir-lintegrite-ou-signer
 */
function hashPassword(password) {
    const saltPassword = password.concat(process.env.SALT);
    return shajs('sha256').update(saltPassword).digest('hex')
}

class User {
    constructor() {
        /** Constructeur : crée un model à partir du schéma */
        this.userModel = mongoose.model('users', userSchema);
    }

    /** Fonction permettant de récupérer l'id de l'utilisateur
     * avec l'adresse e-mail et le mot de passe.
     * La fonction retourne l'id utilisateur si l'adresse et le mot de passe sont trouvés
     * dans la base de données
     * La fonction retourne un message d'erreur si l'un de ces 2 éléments sont erronés ou manquant
     */
    get(email = "", password = "") {
        return new Promise((resolve, reject) => {
            /** Vérification de l'existance 
             * de l'adresse e-mail dans la BDD */
            this.userModel.findOne({ email: email, password: hashPassword(password) }, (err, user) => {
                if (err || user === null) {
                    reject({ message: "Adresse e-mail et/ou mot de passe incorrect(s)" })
                } else {
                    resolve(user.id)
                }
            })
        })
    }

    /** Fonction permettant d'enregistrer un nouvel utilisateur
     * Un controle est effectué sur la cohérence adresse e-mail
     * et sur la complexité du mot de passe
     */
    set(email, password) {
        return new Promise((resolve, reject) => {
            /** Controles sur l'adresse e-mail et le mot de passe */
            const checkedEmail = checkEmail(email);
            const checkedPassword = checkPassword(password);
            if (!checkedEmail) { reject({ message: "Adresse e-mail invalide" }) }
            if (!checkedPassword) { reject({ message: "Mot de passe trop faible. Minimum 8 caractères, dont 1 lettre minuscule, 1 lettre majuscule, 1 chiffre et 1 caractère spécial" }) }
            /** Vérification de l'existance 
             * de l'adresse e-mail dans la BDD */
            if (checkedEmail && checkedPassword) {
                this.userModel.find({ email: email }, (err, users) => {
                    if (users.length === 0) {
                        /** Si elle n'existe pas, on la créé */
                        this.userModel.create({ email: email, password: hashPassword(password) }, (err, data) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve({ message: `Adresse e-mail ${email} créée` })
                            }
                        })
                        /** Sinon, erreur */
                    } else {
                        reject({ message: 'Adresse e-mail existante' })
                    }
                })
            }
        })
    }
}

module.exports = User;