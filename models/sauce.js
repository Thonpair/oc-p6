/** Modèle pour la gestion des sauces dans une base de données MongoDB */

/** Import des dépendances */
const mongoose = require('mongoose')
const { Schema } = mongoose;
require('dotenv').config();
const fs = require("fs");

/** Connexion à la base de données */
mongoose.connect(process.env.DB, { useNewUrlParser: true, useUnifiedTopology: true });

/** Création du schéma pour les sauces */
const SauceSchema = new Schema({
    userId: String,
    name: String,
    manufacturer: String,
    description: String,
    mainPepper: String,
    imageUrl: String,
    heat: Number,
    likes: Number,
    dislikes: Number,
    usersLiked: [],
    usersDisliked: []
})

class Sauce {
    constructor() {
        /** Constructeur : crée un model à partir du schéma */
        this.sauceModel = mongoose.model('sauces', SauceSchema);
    }

    /** Retourne l'ensemble des sauces */
    getAll() {
        return new Promise((resolve, reject) => {
            this.sauceModel.find({}, (err, sauces) => {
                if (err) { reject(err) }
                resolve(sauces)
            })
        })
    }

    /** Retourne une sauce en fonction de son ID */
    getOne(sauceId) {
        return new Promise((resolve, reject) => {
            this.sauceModel.findOne({ _id: sauceId }, (err, sauce) => {
                if (err) { reject(err) }
                resolve(sauce)
            })
        })
    }

    /** Ajout d'une sauce à la base de données */
    add(sauce) {
        return new Promise((resolve, reject) => {
            /** Création d'une sauce à l'aide du schéma */
            this.sauceModel.create({
                userId: sauce.userId,
                name: sauce.name,
                manufacturer: sauce.manufacturer,
                description: sauce.description,
                mainPepper: sauce.mainPepper,
                imageUrl: sauce.imageUrl,
                heat: sauce.heat,
                likes: 0,
                dislikes: 0,
                usersLiked: [],
                usersDisliked: []
            }, (err, sauce) => {
                /** Retourne une erreur en cas d'échec
                 * ou la nouvelle sauce créée si réussite
                 */
                if (err) { reject(err) }
                resolve({ message: JSON.stringify(sauce) })
            })
        })
    }

    /** Met à jour une sauce
     * L'image fournie est optionnelle.
     * Seul le propriétaire de la sauce peut la mettre à jour
     */
    update(sauceId, userId, sauceValues, sauceImgUrl = undefined) {
        /** Objet contenant les valeurs de la nouvelle sauce */
        const updatedSauce = sauceValues;
        /** Si l'image est fournie, on ajoute la sauce à cet objet */
        if (sauceImgUrl !== undefined) {
            updatedSauce.imageUrl = sauceImgUrl
        }
        return new Promise((resolve, reject) => {
            this.sauceModel.findOneAndUpdate(
                /** On cherche la sauce avec l'id de la sauce et l'id utilisateur
                 * Si l'utilisateur n'est pas le propriétaire, aucune sauce 
                 * ne sera trouvée et la mise à jour ne se fera pas
                 */
                { _id: sauceId, userId: userId },
                updatedSauce,
                (err, sauce) => {
                    if (err) { reject(err) }
                    /** Aucune sauce trouvée, ou l'utilisateur n'est pas le propriétaire,
                     * on retourne un message d'erreur
                     */
                    if (sauce === null) { reject({ message: "La sauce n'a pas pu être mise à jour" }) }
                    /** La sauce a été mise à jour, on retourne un message d'informations */
                    else { resolve({ message: "Sauce mise à jour" }) }
                })
        })
    }
    /** Supprime une sauce en fonction de l'id (sauceId)
     * Seul le propriétaire peut la supprimer (userId)
     */
    deleteOne(sauceId, userId) {
        return new Promise((resolve, reject) => {
            this.sauceModel.findOneAndDelete(
                /** On cherche la sauce avec l'id de la sauce et l'id utilisateur
                 * Si l'utilisateur n'est pas le propriétaire, aucune sauce 
                 * ne sera trouvée et la suppression ne se fera pas
                 */
                { _id: sauceId, userId: userId },
                (err, sauce) => {
                    if (err) { reject(err) }
                    /** Aucune sauce trouvée, ou l'utilisateur n'est pas le propriétaire,
                     * on retourne un message d'erreur
                     */
                    if (sauce === null) { reject({ message: "Vous ne pouvez pas supprimer cette sauce" }) }
                    else {
                        /** La suppression a été effectuée dans la base de données
                         * on supprime le fichier
                         */
                        let path = sauce.imageUrl.replace(process.env.URL + ":" + process.env.PORT, '.');
                        fs.unlink(path, (err) => { if (err) { reject(err); } })
                        /** Retourne un message d'informations */
                        resolve({ message: JSON.stringify(sauce) })
                    }
                })
        })
    }

    /** Supprime le like d'un utilisateur sur une sauce */
    _removeLike(sauceId, userId) {
        return new Promise((resolve, reject) => {
            /** Recherche de la sauce avec son id */
            this.getOne(sauceId)
                .then(sauce => {
                    /** Récupération de la liste des utilisateurs ayant aimé la sauce */
                    const usersLiked = sauce.usersLiked;
                    /** Suppression de l'utilisateur fourni (userId) de la liste des utilisateurs */
                    usersLiked.splice(usersLiked.indexOf(userId), 1);
                    /** Mise à jour de la sauce
                     * en décrémentant la valeur du nombre de likes et
                     * en y insérant la nouvelle liste d'utilisateurs ayant aimé la sauce
                     */
                    this.sauceModel.findOneAndUpdate(
                        { _id: sauceId },
                        {
                            $inc: { likes: -1 },
                            usersLiked: usersLiked
                        }, (err, sauce) => {
                            /** Retourne la sauce mise à jour*/
                            if (err) { reject(err) }
                            resolve(sauce)
                        }
                    )
                })
        })

    }

    /** Supprime le dislike d'un utilisateur sur une sauce */
    _removeDislike(sauceId, userId) {
        return new Promise((resolve, reject) => {
            /** Recherche de la sauce avec son id */
            this.getOne(sauceId)
                .then(sauce => {
                    /** Récupération de la liste des utilisateurs n'ayant pas aimé la sauce */
                    const usersDisliked = sauce.usersDisliked;
                    /** Suppression de l'utilisateur fourni (userId) de la liste des utilisateurs */
                    usersDisliked.splice(usersDisliked.indexOf(userId), 1);
                    /** Mise à jour de la sauce
                     * en décrémentant la valeur du nombre de dislikes et
                     * en y insérant la nouvelle liste d'utilisateurs n'ayant pas aimé la sauce
                     */
                    this.sauceModel.findOneAndUpdate(
                        { _id: sauceId },
                        {
                            $inc: { dislikes: -1 },
                            usersDisliked: usersDisliked
                        }, (err, sauce) => {
                            /** Retourne la sauce mise à jour*/
                            if (err) { reject(err) }
                            resolve(sauce)
                        }
                    )
                })
        })
    }

    /** Ajoute le like d'un utilisateur sur une sauce */
    _addLike(sauceId, userId) {
        return new Promise((resolve, reject) => {
            /** Recherche de la sauce avec son id
             * et la met à jour en incrémentant la valeur de like
             * et en ajoutant l'utilisateur à la liste des utisateurs ayant aimé la sauce
             */
            this.sauceModel.findOneAndUpdate(
                { _id: sauceId },
                {
                    $inc: { likes: 1 },
                    $push: { usersLiked: userId }
                }, (err, sauce) => {
                    if (err) { reject(err) }
                    /** Retourne la sauce mise à jour */
                    resolve(sauce)
                }
            )
        })
    }

    /** Ajoute le dislike d'un utilisateur sur une sauce */
    _addDislike(sauceId, userId) {
        return new Promise((resolve, reject) => {
            /** Recherche de la sauce avec son id
 * et la met à jour en incrémentant la valeur de dislike
 * et en ajoutant l'utilisateur à la liste des utisateurs n'ayant pas aimé la sauce
 */
            this.sauceModel.findOneAndUpdate(
                { _id: sauceId },
                {
                    $inc: { dislikes: 1 },
                    $push: { usersDisliked: userId }
                }, (err, sauce) => {
                    if (err) { reject(err) }
                    resolve(sauce)
                }
            )
        })
    }

    /** Met à jour les like ou disklike d'un utilisateur (userId) sur une sauce (sauceId)
     * en fonction du score passé en paramètre
     */
    like(sauceId, userId, score) {
        return new Promise((resolve, reject) => {
            /** Recherche de la sauce */
            this.sauceModel.findOne({ _id: sauceId }, (err, sauce) => {
                if (err) { reject(err) }
                /** Recherche si l'utilisateur a déjà aimé ou non la sauce,
                 * en recherchant dans les tableaux "usersLiked" et "usersDisliked"
                 * si l'userId est présent
                 */
                const userLiked = sauce.usersLiked.includes(userId);
                const userDisliked = sauce.usersDisliked.includes(userId);
                switch (score) {
                    /** Si le score est 0, on supprime le like ou le dislike */
                    case 0:
                        if (userLiked) { this._removeLike(sauceId, userId) };
                        if (userDisliked) { this._removeDislike(sauceId, userId) };
                        break;
                        /** Si le score est 1 et si l'utilisateur n'est pas dans 
                         * la liste des utilisateurs ayant aimé la sauce,
                         * on ajoute le like de l'utilisateur à cette sauce
                         */
                    case 1:
                        if (!userLiked) { this._addLike(sauceId, userId) };
                        break;
                        /** Si le score est -1 et si l'utilisateur n'est pas dans 
                         * la liste des utilisateurs n'ayant pas aimé la sauce,
                         * on ajoute le dislike de l'utilisateur à cette sauce
                         */
                    case -1:
                        if (!userDisliked) { this._addDislike(sauceId, userId) };
                        break;
                }
                /** Retourne la sauce */
                resolve(sauce)
            })
        })
    }


}

module.exports = Sauce;