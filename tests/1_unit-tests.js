const chaiHttp = require('chai-http');
const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const fs = require('fs');
const path = require('path');
chai.use(chaiHttp);
const server = require('../server');

const firstEmail = "firstEmail" + Date.now() + "@example.com"
const firstPassword = "diLXH7@BM6aQ" + Date.now();
const secondEmail = "secondEmail" + Date.now() + "@example.com"
const secondPassword = "aSXP@3i*yy*U" + Date.now();
const badEmail = "email";
const badPassword = "password";
let firstUserId;
let firstToken;
let secondUserId;
let secondToken;

const sauce = {
    name: "Nom de la première sauce",
    manufacturer: "Fabricant de la première sauce",
    description: "Description de la première sauce",
    mainPepper: "Ingrédient principal de la première sauce",
    heat: 5,
    newName: "Nom modifié de la première sauce",
    newManufucturer: "Fabricant modifié de la première sauce",
}

let firstSauceId;
let secondSauceId;
let sauceLenBeforeInsert;
let sauceLenAfterInsert;
const firstImageName = "example.jpg";
const secondImageName = "example2.jpg"
const firstImageContent = fs.readFileSync(path.join(__dirname, firstImageName));
const secondImageContent = fs.readFileSync(path.join(__dirname, secondImageName));


suite('Tests fonctionnels', () => {
    suite('Utilisateur', () => {
        test(`Tentative d'enregistrement avec une adresse e-mail invalide`, (done) => {
            chai.request(server)
                .post('/api/auth/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: badEmail,
                    password: firstPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    assert.equal(res.body.message, "Adresse e-mail invalide");
                    done();
                })
        })

        test(`Tentative d'enregistrement avec un mot de passe trop faible`, (done) => {
            chai.request(server)
                .post('/api/auth/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: firstEmail,
                    password: badPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    assert.equal(res.body.message, "Mot de passe trop faible. Minimum 8 caractères, dont 1 lettre minuscule, 1 lettre majuscule, 1 chiffre et 1 caractère spécial");
                    done();
                })
        })

        test(`Tentative d'enregistrement du premier compte`, (done) => {
            chai.request(server)
                .post('/api/auth/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: firstEmail,
                    password: firstPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.message, `Adresse e-mail ${firstEmail} créée`)
                    done();
                })

        })

        test('Authentification échouée au premier compte', (done) => {
            chai.request(server)
                .post('/api/auth/login')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: firstEmail,
                    password: badPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    assert.equal(res.body.message, 'Adresse e-mail et/ou mot de passe incorrect(s)')
                    done()
                })
        })

        test(`Authentification premier compte`, (done) => {
            chai.request(server)
                .post('/api/auth/login')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: firstEmail,
                    password: firstPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.isString(res.body.userId);
                    assert.isString(res.body.token);
                    firstUserId = res.body.userId;
                    firstToken = res.body.token;
                    done()
                })
        })

        test(`Tentative d'enregistrement du second compte`, (done) => {
            chai.request(server)
                .post('/api/auth/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: secondEmail,
                    password: secondPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.message, `Adresse e-mail ${secondEmail} créée`)
                    done();
                })
        })

        test('Authentification du second compte', (done) => {
            chai.request(server)
                .post('/api/auth/login')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: secondEmail,
                    password: secondPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.isString(res.body.userId);
                    assert.isString(res.body.token);
                    secondUserId = res.body.userId;
                    secondToken = res.body.token;
                    done()
                })
        })

        test(`Tentative d'enregistrement une seconde fois du premier compte`, (done) => {
            chai.request(server)
                .post('/api/auth/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: firstEmail,
                    password: firstPassword
                })
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    assert.equal(res.body.message, `Adresse e-mail existante`)
                    done();
                })
        })
    })

    suite("Sauce", () => {
        test(`Utilisateur 1 - avant création de sauces : Affichage de toutes les sauces`, (done) => {
            chai.request(server)
                .get('/api/sauces')
                .set({ Authorization: `Bearer ${firstToken}` })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.isArray(res.body)
                    const sauces = res.body;
                    sauceLenBeforeInsert = res.body.length
                    sauces.map(sauce => {
                        assert.isString(sauce.userId);
                        assert.isString(sauce.name);
                        assert.isString(sauce.manufacturer);
                        assert.isString(sauce.description);
                        assert.isString(sauce.mainPepper);
                        assert.isString(sauce.imageUrl);
                        assert.isNumber(sauce.heat);
                        assert.isNumber(sauce.likes);
                        assert.isNumber(sauce.dislikes);
                        assert.isArray(sauce.usersLiked);
                        assert.isArray(sauce.usersDisliked);
                        assert.isString(sauce._id);
                    })
                    done()
                })
        })

        test(`Utilisateur 1 : Création d'une sauce`, (done) => {
            chai.request(server)
                .post('/api/sauces/')
                .set({ Authorization: `Bearer ${firstToken}` })
                .set('content-type', 'application/x-www-form-urlencoded')
                .attach('image', firstImageContent, firstImageName)
                .field("sauce", '{"name":"' + sauce.name + '",' +
                    '"manufacturer":"' + sauce.manufacturer + '",' +
                    '"description":"' + sauce.description + '",' +
                    '"mainPepper":"' + sauce.mainPepper + '",' +
                    '"heat":' + sauce.heat + ',"userId":"' + firstUserId + '"}')
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.isString(res.body.message);
                    const resMessage = JSON.parse(res.body.message);
                    assert.equal(resMessage.userId, firstUserId);
                    assert.equal(resMessage.name, sauce.name);
                    assert.equal(resMessage.manufacturer, sauce.manufacturer);
                    assert.equal(resMessage.description, sauce.description);
                    assert.equal(resMessage.mainPepper, sauce.mainPepper);
                    assert.isTrue(/https?:\/\/.*\/example-\d{13,}.jpg/.test(resMessage.imageUrl));
                    assert.equal(resMessage.heat, sauce.heat);
                    assert.equal(resMessage.likes, 0);
                    assert.equal(resMessage.dislikes, 0);
                    assert.isArray(resMessage.usersLiked);
                    assert.lengthOf(resMessage.usersLiked, 0);
                    assert.isArray(resMessage.usersDisliked);
                    assert.lengthOf(resMessage.usersDisliked, 0);
                    assert.isString(resMessage._id);
                    firstSauceId = resMessage._id;
                    done()
                })
        })

        test(`Utilisateur 2 : Création d'une sauce`, (done) => {
            chai.request(server)
                .post('/api/sauces/')
                .set({ Authorization: `Bearer ${secondToken}` })
                .set('content-type', 'application/x-www-form-urlencoded')
                .attach('image', firstImageContent, firstImageName)
                .field("sauce", '{"name":"' + sauce.name + '",' +
                    '"manufacturer":"' + sauce.manufacturer + '",' +
                    '"description":"' + sauce.description + '",' +
                    '"mainPepper":"' + sauce.mainPepper + '",' +
                    '"heat":' + sauce.heat + ',"userId":"' + secondUserId + '"}')
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.isString(res.body.message);
                    const resMessage = JSON.parse(res.body.message);
                    assert.equal(resMessage.userId, secondUserId);
                    assert.equal(resMessage.name, sauce.name);
                    assert.equal(resMessage.manufacturer, sauce.manufacturer);
                    assert.equal(resMessage.description, sauce.description);
                    assert.equal(resMessage.mainPepper, sauce.mainPepper);
                    assert.isTrue(/https?:\/\/.*\/example-\d{13,}.jpg/.test(resMessage.imageUrl));
                    assert.equal(resMessage.heat, sauce.heat);
                    assert.equal(resMessage.likes, 0);
                    assert.equal(resMessage.dislikes, 0);
                    assert.isArray(resMessage.usersLiked);
                    assert.lengthOf(resMessage.usersLiked, 0);
                    assert.isArray(resMessage.usersDisliked);
                    assert.lengthOf(resMessage.usersDisliked, 0);
                    assert.isString(resMessage._id);
                    secondSauceId = resMessage._id;
                    done()
                })
        })

        test(`Utilisateur 1 - après création de création : Affichage de toutes les sauces`, (done) => {
            chai.request(server)
                .get('/api/sauces')
                .set({ Authorization: `Bearer ${firstToken}` })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.isArray(res.body)
                    const sauces = res.body;
                    sauceLenAfterInsert = res.body.length
                    sauces.map(sauce => {
                        assert.equal(sauceLenAfterInsert, sauceLenBeforeInsert + 2)
                        assert.isString(sauce.userId);
                        assert.isString(sauce.name);
                        assert.isString(sauce.manufacturer);
                        assert.isString(sauce.description);
                        assert.isString(sauce.mainPepper);
                        assert.isString(sauce.imageUrl);
                        assert.isNumber(sauce.heat);
                        assert.isNumber(sauce.likes);
                        assert.isNumber(sauce.dislikes);
                        assert.isArray(sauce.usersLiked);
                        assert.isArray(sauce.usersDisliked);
                        assert.isString(sauce._id);
                    })
                    done()
                })
        })

        test(`Utilisateur 1 : Affichage d'une sauce`, (done) => {
            chai.request(server)
                .get('/api/sauces/' + firstSauceId)
                .set({ Authorization: `Bearer ${firstToken}` })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.userId, firstUserId);
                    assert.equal(res.body.name, sauce.name);
                    assert.equal(res.body.manufacturer, sauce.manufacturer);
                    assert.equal(res.body.description, sauce.description);
                    assert.equal(res.body.mainPepper, sauce.mainPepper);
                    assert.isTrue(/https?:\/\/.*\/example-\d{13,}.jpg/.test(res.body.imageUrl));
                    assert.equal(res.body.heat, sauce.heat);
                    assert.equal(res.body.likes, 0);
                    assert.equal(res.body.dislikes, 0);
                    assert.lengthOf(res.body.usersLiked, 0);
                    assert.lengthOf(res.body.usersDisliked, 0);
                    assert.equal(res.body._id, firstSauceId);
                    done()
                })
        })

        test(`Utilisateur 1 : Modification d'une sauce lui appartenant : seulement texte`, (done) => {
            chai.request(server)
                .put('/api/sauces/' + firstSauceId)
                .set({ Authorization: `Bearer ${firstToken}` })
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: sauce.newName,
                    manufacturer: sauce.manufacturer,
                    description: sauce.description,
                    mainPepper: sauce.mainPepper,
                    heat: sauce.heat,
                    userId: firstUserId
                })
                .end((err, res) => {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.message, "Sauce mise à jour");
                    done();
                })
        })

        test(`Utilisateur 1 : Modification d'une sauce lui appartenant : texte + image`, (done) => {
            chai.request(server)
                .put('/api/sauces/' + firstSauceId)
                .set({ Authorization: `Bearer ${firstToken}` })
                .attach('image', secondImageContent, secondImageName)
                .set('content-type', 'application/x-www-form-urlencoded')
                .field("sauce", '{"name":"' + sauce.newName + '",' +
                    '"manufacturer":"' + sauce.newManufucturer + '",' +
                    '"description":"' + sauce.description + '",' +
                    '"mainPepper":"' + sauce.mainPepper + '",' +
                    '"heat":' + sauce.heat + ',"userId":"' + firstUserId + '"}')
                .end((err, res) => {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.message, "Sauce mise à jour");
                    done();
                })
        })

        test(`Utilisateur 1 : Modification échouée d'une sauce ne lui appartenant pas`, (done) => {
            chai.request(server)
                .put('/api/sauces/' + secondSauceId)
                .set({ Authorization: `Bearer ${firstToken}` })
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: sauce.name,
                    manufacturer: sauce.manufacturer,
                    description: sauce.description,
                    mainPepper: sauce.mainPepper,
                    heat: sauce.heat,
                    userId: firstUserId
                })
                .end((err, res) => {
                    assert.equal(res.status, 403)
                    assert.equal(res.body.err.message, "La sauce n'a pas pu être mise à jour");
                    done();
                })
        })

        test(`Utilisateur 1 : Suppression d'une sauce lui appartenant`, (done) => {
            chai.request(server)
                .delete('/api/sauces/' + firstSauceId)
                .set({ Authorization: `Bearer ${firstToken}` })
                .end((err, res) => {
                    assert.equal(res.status, 200)
                    assert.isString(res.body.message);
                    const resMessage = JSON.parse(res.body.message);
                    assert.equal(resMessage.userId, firstUserId);
                    assert.equal(resMessage.name, sauce.newName);
                    assert.equal(resMessage.manufacturer, sauce.newManufucturer);
                    assert.equal(resMessage.description, sauce.description);
                    assert.equal(resMessage.mainPepper, sauce.mainPepper);
                    assert.isTrue(/https?:\/\/.*\/example2-\d{13,}.jpg/.test(resMessage.imageUrl));
                    assert.equal(resMessage.heat, sauce.heat);
                    assert.equal(resMessage.likes, 0);
                    assert.equal(resMessage.dislikes, 0);
                    assert.lengthOf(resMessage.usersLiked, 0);
                    assert.lengthOf(resMessage.usersDisliked, 0);
                    assert.equal(resMessage._id, firstSauceId);
                    done()
                })
        })

        test(`Utilisateur 1 : Suppression échouée d'une sauce ne lui appartenant pas`, (done) => {
            chai.request(server)
                .delete('/api/sauces/' + secondSauceId)
                .set({ Authorization: `Bearer ${firstToken}` })
                .end((err, res) => {
                    assert.equal(res.status, 403)
                    assert.equal(res.body.err.message, 'Vous ne pouvez pas supprimer cette sauce')
                    done()
                })
        })

        test(`Utilisateur 2 : Suppression d'une sauce lui appartenant`, (done) => {
            chai.request(server)
                .delete('/api/sauces/' + secondSauceId)
                .set({ Authorization: `Bearer ${secondToken}` })
                .end((err, res) => {
                    assert.equal(res.status, 200)
                    assert.isString(res.body.message);
                    const resMessage = JSON.parse(res.body.message);
                    assert.equal(resMessage.userId, secondUserId);
                    assert.equal(resMessage.name, sauce.name);
                    assert.equal(resMessage.manufacturer, sauce.manufacturer);
                    assert.equal(resMessage.description, sauce.description);
                    assert.equal(resMessage.mainPepper, sauce.mainPepper);
                    assert.isTrue(/https?:\/\/.*\/example-\d{13,}.jpg/.test(resMessage.imageUrl));
                    assert.equal(resMessage.heat, sauce.heat);
                    assert.equal(resMessage.likes, 0);
                    assert.equal(resMessage.dislikes, 0);
                    assert.lengthOf(resMessage.usersLiked, 0);
                    assert.lengthOf(resMessage.usersDisliked, 0);
                    assert.equal(resMessage._id, secondSauceId);
                    done()
                })
        })
    })
})
