/** Controleur pour la gestion des utilisateurs : 
 * Authentification et inscription
 */

/** Import des dépendances */
const jwt = require('jsonwebtoken');
require('dotenv').config();

/** Import du modèle utilisateur */
const User = require('../models/user');

/** Fonction pour l'inscription
 * Récupère l'adresse e-mail et le mot de passe depuis la requête fournie
 * puis crée un nouvel utilisateur avec ces éléments
 */
exports.signup = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    new User().set(email, password)
        .then(data => { res.status(200).json(data) })
        .catch(err => { res.status(401).json(err) })
}

/** Fonction pour l'authentification de l'utilisateur
 * Récupère l'adresse e-mail et le mot de passe depuis la requête fournie
 * puis vérifie que ces éléments sont corrects.
 * Si les identifiants sont incorrects, un message d'erreur est renvoyé.
 * Si les indentifiants sont corrects, la fonction créé et retourne un token d'authentification
 */
exports.login = (req, res, next) => {
    /** Récupération des informations */
    const email = req.body.email;
    const password = req.body.password;
    /** Controle des identifiants dans la base de données, via le modèle User */
    new User().get(email, password)
        /** Retour du modèle */
        .then(data => {
            /** Les identifiants sont incorrects : retour d'un message d'erreur */
            /** Les identifiants sont corrects : création et envoi d'un token d'authentification */
            const token = {
                userId: data,
                token: jwt.sign(
                    { userId: data },
                    process.env.AUTH_SECRET,
                    { expiresIn: '24h' }
                )
            };
            res.status(200).json(token)
        }).catch(err => { res.status(401).json(err) })
}
