/** Controleur pour la gestion des sauces */

/** Import des dépendances */
const Sauce = require('../models/sauce');
require('dotenv').config();
const path = require("path");

/** Fonction interne pour créer un chemin d'accès externe pour les fichiers images */
const externalFilePath = (filePath) => {    
    return process.env.URL + ':' + path.join(process.env.PORT, filePath);
}

/** Création d'une sauce selon les informations fournies  */
exports.create = (req, res, next) => {
    /** Création d'un objet sauce depuis le modèle */
    const sauce = new Sauce();
    /** Récupération des informations */
    const newSauce = JSON.parse(req.body.sauce)
    /** Récupération et génération du lien vers l'image */
    newSauce.imageUrl = externalFilePath(req.file.path);
    /** Création de la sauce depuis l'objet sauce */
    sauce.add(newSauce)
        .then(sauce => { res.status(200).json(sauce); })
        .catch(err => { res.status(401).json({ err }) })
}

/**Récupèration des sauces
 * S'il n'y a pas d'id fourni, l'ensemble des sauces est retourné,
 * Sinon, seule la sauce demandée via son id est renvoyée
 */
exports.read = (req, res, next) => {
    const sauce = new Sauce();
    /** Récupère l'id de la sauce */
    const sauceId = req.params.id;
    /** S'il n'y a pas d'id envoyé, on récupère l'ensemble des sauces */
    if (sauceId === undefined) {
        sauce.getAll()
            .then(sauceList => { res.status(200).json(sauceList); })
            .catch(err => { res.status(401).json({ err }) })
    } else {
        /** Sinon, on récupère la sauce avec l'id correspondant */
        sauce.getOne(sauceId)
            .then(sauce => { res.status(200).json(sauce); })
            .catch(err => { res.status(401).json({ err }) })
    }
}
/** Mise à jour des sauces*/
exports.update = (req, res, next) => {
    const sauce = new Sauce();
    /** Récupération de l'id de la sauce depuis l'url */
    const sauceId = req.params.id;
    /** Récupération de l'userId fourni par le middleware d'authentification */
    const userId = req.userId;
    /** Définition des valeurs par défaut à mettre à jour  */
    let imageUrl = undefined
    let updatedSauce = req.body;
    /** Détection d'une image, changement des valeurs à mettre à jour */
    if (req.file !== undefined) {
        imageUrl = externalFilePath(req.file.path)
        updatedSauce = JSON.parse(req.body.sauce)
    }
    /** Mise à jour des valeurs */
    sauce.update(sauceId, userId, updatedSauce, imageUrl)
        .then(sauce => {
            res.status(200).json(sauce);
        })
        .catch(err => { res.status(403).json({ err }) })
}

/** Supprime une sauce */
exports.delete = (req, res, next) => {
    const sauce = new Sauce();
    /** Récupération de l'id de la sauce depuis l'url */
    const sauceId = req.params.id;
    /** Récupération de l'userId fourni par le middleware d'authentification */
    const userId = req.userId;
    /** Lance la suppression de la sauce */ 
    sauce.deleteOne(sauceId, userId)
        .then(sauce => res.status(200).json(sauce))
        .catch(err => res.status(403).json({ err }))
}

/** Met à jour les "j'aime" ou "je n'aime pas" */
exports.like = (req, res, next) => {
    const sauce = new Sauce();
    sauce.like(req.params.id, req.body.userId, req.body.like)
        .then(sauce => res.status(200).json(sauce))
        .catch(err => { console.log(err); res.status(401).json({ err }) })
}
