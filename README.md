# OpenClassrooms - Projet 6 - Piiquante

## Installation

Pour lancer l'installation du projet, lancer la commande

> npm run start

## Tests unitaires

Pour lancer les tests unitaires, lancer la commande

> npm run test

## Exécution

Pour exécuter le projet, lancer la commande 

> npm run start

## Configuration

Les différents paramètres sont à insérer dans le fichier .env à la racine du projet.

### Description du fichier de configuration

* PORT : port d'écoute de l'interface 
* URL : url de l'api
* AUTH_SECRET : clé secrète pour l'authentification par jeton
* DB : uri de la base de données MongoDB
* SALT : clé secrète ajoutée au mot de passe avant chiffrement dans la base de données
* UPLOAD_DIR : chemin vers lequel les images seront stockées



