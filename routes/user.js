/**Middleware gérant l'authentification */

/** Import des dépendances */
const express = require('express');
const router = express.Router();

/** Import du controleur */
const userCtrl = require('../controllers/user');

/** Les différentes routes */
router.post('/signup', userCtrl.signup);
router.post('/login', userCtrl.login);

module.exports = router;