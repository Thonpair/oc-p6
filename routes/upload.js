/** Gestion des routes pour accéder aux images */

/** Import des dépendances */
const express = require('express');
const router = express.Router();
/** Import du controleur */
const uploadCtrl = require('../controllers/upload');
/** Route pour accéder aux fichiers */
router.get('/:id', uploadCtrl.get);

module.exports = router;