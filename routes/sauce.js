/** Gestion des routes pour les appels aux sauces */

/** Import des dépendances */
const express = require('express');
const router = express.Router();
/** Import des middlewares d'authentification et de stockage de fichier */
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');
/** Import du controleur des sauces */
const sauceCtrl = require('../controllers/sauce')

/** Les différentes routes pour accéder aux sauces
 * Elles sont toutes authentifiées via le middleware auth
 * Les fichiers sont interceptés via le middleware multer
 */
router.get('/', auth, sauceCtrl.read);
router.get('/:id', auth, sauceCtrl.read);
router.post('/', auth, multer, sauceCtrl.create);
router.put('/:id', auth, multer, sauceCtrl.update);
router.delete('/:id', auth, sauceCtrl.delete);
router.post('/:id/like', auth, sauceCtrl.like);

module.exports = router;