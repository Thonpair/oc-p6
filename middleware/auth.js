/** Middleware gérant les jetons d'authentification
 * Ce middleware est à utiliser pour chaque requête nécessitant une authentification.
 */

/** Import des dépendances */
const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = (req, res, next) => {
  try {
    /** Récupération de l'identifiant utilisateur (req.body.userId) et du token envoyés */
    let user = req.body.userId;
    const token = req.headers.authorization.split(' ')[1];
    /** Décodage du token avec la clé de chiffrement */
    const decodedToken = jwt.verify(token, process.env.AUTH_SECRET);
    /** Extraction de l'identifiant utilisateur depuis le token */
    const userId = decodedToken.userId.toString();
    /** Si requête GET ou DELETE, il n'y a pas de body dans la requête ; user vaudra undefined
     * On vérifie que l'userId corresponde à l'user fourni, s'il est renseigné
     */
    
    if (user && user !== userId) {
      res.status(401).json({ message: "erreur d'authentification - jeton invalide" });
    } else {
      /** affectation de l'userId à req */
      req.userId = userId
      next();
    }
  } catch (err) {
    res.status(401).json({ message: err });
  }
};