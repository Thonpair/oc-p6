/** Middleware gérant la capture et le stockage 
 * de fichiers image dans le flux de données
 */

/** Import des dépendances */
const multer = require('multer');
const fs = require('fs');
require('dotenv').config();

/** types de fichiers acceptés */
const MIME_TYPES = {
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg',
  'image/png': 'png'
};

/** Chemin d'accès où les images seront stockées */
const uploadDir = '.' + process.env.UPLOAD_DIR;

/**Création du dossier de destination s'il n'existe pas */
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

/** Configuration du disque virtuel de stockage ainsi que les fichiers */
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, uploadDir);
  },
  filename: (req, file, callback) => {
    /** Stocke les fichiers en ajoutant la date, pour éviter tout doublon */
    const extension = MIME_TYPES[file.mimetype];
    const name = file.originalname.split(' ').join('_').split('.' + extension)[0];
    callback(null, name + '-' + Date.now() + '.' + extension);
  }
});

module.exports = multer({ storage: storage }).single('image');